# processing-gradle-example

This project serves as an example Java [Processing](https://processing.org/) application which is built with [Gradle](https://gradle.org/). You can use it as help and reference to create your own project. Please do not clone this repository and rename its files. Setup a new clean project as described below if you want to create your own application.

## Prerequisites

- [OpenJDK](https://openjdk.java.net/)
- [Gradle](https://gradle.org/)

For Java 16 you need at least [Gradle 7.0](https://gradle.org/release-candidate/).

## Setup

How to add the Processing library using Gradle. This is already done in this project.

1. Setup a Java project as described [here](https://git.thm.de/dhzb87/JbX/-/blob/master/GradleJava.md)
2. Copy the `core.jar` file from [Processing](https://processing.org/download/) (or from this repo) to the `app` directory
3. Open `src/build.gradle`
4. Add `implementation files('core.jar')` under `dependencies`
5. Start the application with `gradle run`

## Setup IntelliJ

You can download IntelliJ from [here](https://www.jetbrains.com/de-de/idea/). The Ultimate version is free for students.

1. Open IntelliJ and import the project
2. Click on `Add configuration` at the actionbar
3. Click on the `+` symbol and choose `Gradle`
4. Go to `Gradle project` and choose the project's directory
5. Go to `Tasks` and enter `run`
6. Apply & save
7. Click on the green Play-Button to start the application

You can also open the Gradle tab on the right sidebar and choose `Tasks -> application -> run`.

If IntelliJ does not find the installation directory of Gradle do the following:

1. Go to `File -> Settings -> Build, Execution, Deployment -> Build Tools -> Gradle`
2. Go to `Use gradle from` and select `Specified location`
3. Choose Gradle's installation directory on your computer

## Gradle Commands

Read more [here](https://docs.gradle.org/current/userguide/command_line_interface.html#common_tasks).

- `gradle build` - Assemble all outputs and run checks
- `gradle run` - Assemble the application and execute
- `gradle check` - Run tests and linting
- `gradle clean` - Clear the `build` directory
